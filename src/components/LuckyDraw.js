import { Component } from "react";
import "../App.css"
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
class LuckyDraw extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ball1: 0,
            ball2: 0,
            ball3: 0,
            ball4: 0,
            ball5: 0,
            ball6: 0,
            random: 0,
        }
    }
    buttonHandler = () => {
        let random1 = Math.floor(Math.random() * 99) + 1;
        let random2 = Math.floor(Math.random() * 99) + 1;
        let random3 = Math.floor(Math.random() * 99) + 1;
        let random4 = Math.floor(Math.random() * 99) + 1;
        let random5 = Math.floor(Math.random() * 99) + 1;
        let random6 = Math.floor(Math.random() * 99) + 1;
        this.setState({
            ball1: this.state.ball1 = random1,
            ball2: this.state.ball2 = random2,
            ball3: this.state.ball3 = random3,
            ball4: this.state.ball4 = random4,
            ball5: this.state.ball5 = random5,
            ball6: this.state.ball6 = random6,
        })
    }
    render() {
        return (
            <div className="container text-center mt-5">
                <div>
                    <h1>Lucky Draw</h1>
                </div>
                <br></br>
                <div className="col-12">
                    <div className="row" style={{ marginLeft: "280px" }}>
                        <div className="luckyBall col-2" style={{ width: "100px" }}>
                            <p className="numBall">{this.state.ball1}</p>
                        </div>
                        <div className="luckyBall col-2" style={{ width: "100px" }}>
                            <p className="numBall">{this.state.ball2}</p>
                        </div>
                        <div className="luckyBall col-2" style={{ width: "100px" }}>
                            <p className="numBall">{this.state.ball3}</p>
                        </div>
                        <div className="luckyBall col-2" style={{ width: "100px" }}>
                            <p className="numBall">{this.state.ball4}</p>
                        </div>
                        <div className="luckyBall col-2" style={{ width: "100px" }}>
                            <p className="numBall"> {this.state.ball5}</p>
                        </div>
                        <div className="luckyBall col-2" style={{ width: "100px" }}>
                            <p className="numBall" >{this.state.ball6}</p>
                        </div>
                    </div>
                    <br></br>
                    <div className="col-12 text-center">
                        <button style={{
                            border: "1px solid black", backgroundColor: "purple",
                            width: "150px", height: "50px", color: "white", borderRadius: "5px",
                        }}
                            onClick={this.buttonHandler}>Generate</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default LuckyDraw;