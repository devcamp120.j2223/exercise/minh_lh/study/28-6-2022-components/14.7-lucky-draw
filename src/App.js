import LuckyDraw from "../src/components/LuckyDraw"

function App() {
  return (
    <div>
      <LuckyDraw />
    </div>
  );
}

export default App;
